package org.torproject.metrics.descriptorparser.utils;

import org.torproject.descriptor.DescriptorParseException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;



public class DescriptorUtils {
  private static ObjectMapper objectMapper = new ObjectMapper();

  private static final Logger logger = LoggerFactory.getLogger(
      DateTimeHelper.class);

  /**
   * Return a string representation of an object.
   */
  public String fieldAsString(Object field) {
    String out = "";
    try {
      out = this.objectMapper.writeValueAsString(field);
    } catch (JsonProcessingException e) {
      logger.warn("Exception. {}".format(e.getMessage()));
    }
    return out;
  }

  /**
   * Convert a list to array object.
   */
  public Object[] listToArray(List listArray) {
    if (listArray == null) {
      listArray = new ArrayList();
    }
    return listArray.toArray();
  }

  /**
   * Calculate a SHA-256 digest of a descriptor and return its representation
   * in Base64.
   */
  public String calculateDigestSha256Base64(byte[] rawDescriptorBytes)
       throws DescriptorParseException {

    String digestSha256Base64 = null;
    digestSha256Base64 = Base64.encodeBase64String(
        messageDigest("SHA-256", rawDescriptorBytes))
        .replaceAll("=", "");

    if (null == digestSha256Base64) {
      throw new DescriptorParseException("Could not calculate descriptor "
          + "digest.");
    }
    return digestSha256Base64;
  }

  private byte[] messageDigest(String alg, byte[] rawDescriptorBytes) {
    try {
      MessageDigest md = MessageDigest.getInstance(alg);
      md.update(rawDescriptorBytes);
      return md.digest();
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }

}
