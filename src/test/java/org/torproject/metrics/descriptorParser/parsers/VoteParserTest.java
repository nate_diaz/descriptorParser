package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class VoteParserTest {

  @Test()
  public void testVoteParserDbUploader() throws Exception {
    VoteParser vp = new VoteParser();
    String consensusVotePath =
        "src/test/resources/network-status-vote";
    String confFile = "src/test/resources/config.properties.test";
    String networkStatusVoteDigest
        = "vbQcS8prMQGj1fee0ZPYJx0CR+egTe7RyDYMhaMWlZo";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    vp.run(consensusVotePath, conn);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM network_vote WHERE digest = '"
        + networkStatusVoteDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      while (rs.next()) {
        assertEquals(rs.getString("digest"), networkStatusVoteDigest);
        assertEquals(rs.getString("consensus_methods"), "[28,29,30,31,32,33]");
      }
    }
  }
}
