package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class RouterStatusBuilderTest {

  @Test()
  public void testRouterStatusBuilder() throws Exception {
    String confFile = "src/test/resources/config.properties.test";
    String fingerprint = "22F74E176F803499D4F80D9CE7D325883A8C0E45";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM server_status WHERE fingerprint = '"
        + fingerprint + "' LIMIT 1;");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      while (rs.next()) {
        assertEquals(rs.getString("nickname"), "MakeSecure");
        assertEquals(rs.getString("fingerprint"),
            fingerprint);
      }
    }

  }

  @Test()
  public void testInsertingStatusesAtDifferentTime() throws Exception {
    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    String confFile = "src/test/resources/config.properties.test";
    conn = psqlConn.connect(confFile);

    String consensusPathSt =
        "src/test/resources/2023-02-05-20-00-00-consensus";
    String consensusPathNd =
        "src/test/resources/2023-02-02-00-00-00-consensus";
    ConsensusParser cp = new ConsensusParser();
    cp.run(consensusPathSt, conn);
    cp.run(consensusPathNd, conn);

    String serverPath = "src/test/resources/server-descriptors-through-time";
    ServerDescriptorParser sp = new ServerDescriptorParser();
    sp.run(serverPath, conn);

    String fingerprint = "22F74E176F803499D4F80D9CE7D325883A8C0E45";
    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM server_status WHERE fingerprint = '"
        + fingerprint + "' AND published<='2023-02-05 12:00:02' ORDER BY"
        + " published DESC LIMIT 1;");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("nickname"), "MakeSecure");
        assertEquals(rs.getString("fingerprint"),
            fingerprint);
        assertEquals(rs.getTimestamp("last_seen"),
            Timestamp.valueOf("2023-02-05 12:00:02"));
      } else {
        fail("Status not found");
      }
    }

  }

  @Test()
  public void testInsertingBridgesStatuses() throws Exception {
    String confFile = "src/test/resources/config.properties.test";
    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    String consensusPathForVersions =
        "src/test/resources/2022-08-30-10-00-00-consensus";
    ConsensusParser cp = new ConsensusParser();
    cp.run(consensusPathForVersions, conn);

    String serverPath =
        "src/test/resources/bridges-server-desc";
    ServerDescriptorParser sp = new ServerDescriptorParser();
    sp.run(serverPath, conn);

    String fingerprint = "00B5DE0522A6F215BF694DE36576AF1A3927D6D8";
    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM server_status WHERE fingerprint='"
        + fingerprint + "' AND published<='2022-08-30 23:54:51' ORDER BY"
        + " published DESC LIMIT 1;");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("nickname"), "wolf359");
        assertEquals(rs.getString("fingerprint"),
            fingerprint);
        assertEquals(rs.getTimestamp("last_seen"),
            Timestamp.valueOf("2022-08-30 11:54:43"));
      } else {
        fail("Status not found");
      }
    }
  }

}
