DescriptorParser is a small standalone java app to import Tor network descriptors
into an postgresql DB with timescale plugin.

It assumes the same DB schema as defined in (src/main/sql)[https://gitlab.torproject.org/hiro/descriptorParser/-/tree/main/src/main/sql]

You can run the timescaledb locally with docker. You will need this running to
be able to test your code locally. You will need to run the two scripts under
``src/test/resources/bin``.

Run: ``$ ./src/test/resources/bin/runDB`` to run the docker container. Depending
on your setup you might have to sudo in order to run the docker daemon.

Run: ``$ ./src/test/resoucers/bin/createDB`` to create a new database without data.

You might have to run the above script everytime you run the tests.

To start developing on DescriptorParser you can start reading the Java guide
common to all metrics projects:

https://gitlab.torproject.org/tpo/network-health/team/-/wikis/metrics/Java

Make sure you either clone the repository using the ``--recursive`` flag or sync
the metrics base submodule.

To build the project here are some useful ant task:

- ant resolve (to fetch dependencies)
- ant fetch-metrics-lib (to fetch metrics library)
- ant jar (to build the project jar)

The ``run`` script under bin runs the jar for you.

The app expects descriptors to be place in the following folders under the
project home. Ex:
- /descriptors/relay-descriptors/extra-info
- /descriptors/relay-descriptors/server

Descriptors can be individual text files as downloaded from:
- https://collector.torproject.org/recent/

Or archive tarballs that don't have to be extracted before using (the parser
  will just do the heavy lifting for you).

DescriptorParser is still extremely hackish, use at your own risk! xD
